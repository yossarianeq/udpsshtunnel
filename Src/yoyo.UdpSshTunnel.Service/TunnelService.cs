﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net.Config;
using log4net.Ext.EventID;
using Renci.SshNet;
using Renci.SshNet.Common;
using yoyo.UdpSshTunnel.Service.Properties;

namespace yoyo.UdpSshTunnel.Service
{
    class TunnelService : ServiceBase
    {
        private static readonly IEventIDLog _log = EventIDLogManager.GetLogger(typeof (TunnelService));
        private readonly ManualResetEvent _shutdownEvent = new ManualResetEvent(false);
        private Thread _backgroundThread;
        private SshClient _sshc;
        private UdpTunnelClient _udpClient;

        static void Main(string[] args)
        {
            XmlConfigurator.ConfigureAndWatch(new FileInfo(typeof(TunnelService).Assembly.Location + ".config"));

            
            var service = new TunnelService();
            if (args.Contains("/noservice"))
            {
                Console.CancelKeyPress += (sender, eventArgs) => service.Stop();
                service.DoStart();
                service._backgroundThread.Join();
            }
            else
            {
                ServiceBase.Run(new ServiceBase[] {service});
            }
        }

        private void DoStart()
        {
            _log.Info("Service started");
            _backgroundThread = new Thread(ThreadWorker);
            _backgroundThread.Start();
        }

        private SshCommand GetNcCommand()
        {
            return _sshc.CreateCommand("nc -w 1 -u localhost " + Settings.Default.RemotePort.ToString());
        }

        private void ThreadWorker()
        {
            try
            {
                SetupLocalUdp();

                var connectionInfo = new PasswordConnectionInfo(Settings.Default.RemoteHost,
                    Settings.Default.RemoteHostPort, Settings.Default.RemoteUser, Settings.Default.RemotePass);


                using (_sshc = new SshClient(connectionInfo))
                {
                    _sshc.Connect();
                    _shutdownEvent.WaitOne();
                }
            }
            catch (SshAuthenticationException sex)
            {
                _log.Error("Invalid username/password combination. Terminating.", sex);
                Stop();
            }
            catch (Exception ex)
            {
                _log.Fatal("Unexpected exception caught, terminating", ex);
                Stop();
            }
        }

        private void SetupLocalUdp()
        {
            _udpClient = new UdpTunnelClient(Settings.Default.LocalPort);
            _udpClient.ProcessData += ProcessData;
        }

        private byte[] ProcessData(byte[] bs)
        {
            using (var cmd = GetNcCommand())
            {
                cmd.InputData = bs;
                var eex = cmd.ExecuteNoString();

                byte[] rv = new byte[eex.Length];
                eex.Read(rv, 0, (int)eex.Length);

                var err = (cmd.Error);
                if (err.Length > 0)
                {
                    _log.Error("Failed, error: " + err);
                    return null;
                }

                return rv;
            }
        }

        protected override void OnStart(string[] args)
        {
            DoStart();
        }

        protected override void OnStop()
        {
            DoStop();
        }

        private void DoStop()
        {
            _log.Info("Service stopped");
            _shutdownEvent.Set();
        }
    }
}
