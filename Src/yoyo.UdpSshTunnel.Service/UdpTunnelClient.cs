﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace yoyo.UdpSshTunnel.Service
{
    class UdpTunnelClient
    {
        private UdpClient _socket;
        private int _port;

        public delegate byte[] ProcessDataDelegate(byte[] bs);

        public event ProcessDataDelegate ProcessData;

        protected virtual byte[] OnProcessData(byte[] bs)
        {
            ProcessDataDelegate handler = ProcessData;
            if (handler != null) return handler(bs);
            else
            {
                return null;
            }
        }

        private void RecreateSocket()
        {
            if (_socket != null)
            {
                _socket.Close();
            }

            var ipep = new IPEndPoint(IPAddress.Any, _port);
            _socket = new UdpClient(ipep);
            _socket.BeginReceive(OnReceive, null);
            
        }

        public UdpTunnelClient(int port)
        {
            _port = port;
            RecreateSocket();
        }



        private void OnReceive(IAsyncResult ar)
        {
            IPEndPoint remote = null;
            var data = _socket.EndReceive(ar, ref remote);

            var ret = OnProcessData(data);
            if (ret != null)
            {
                _socket.Send(ret, ret.Length, remote);
            }

            try
            {
                _socket.BeginReceive(OnReceive, null);
            }
            catch (SocketException)
            {
                RecreateSocket();
            }
        }
    }
}
